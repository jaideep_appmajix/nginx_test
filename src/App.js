import React from "react";
import { Route, Switch } from "react-router";
import Capp from "./capp";

const App = () => {
  const PageN = () => <div>page Not Found</div>;
  return (
    <div>
      <Switch>
        <Route path="/" component={Capp} />
        <Route path="/posts" component={Capp} />
        <Route component={PageN} />
      </Switch>
    </div>
  );
};

export default App;
